//jshint esversion:6
require('dotenv').config();
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const mongoose = require('mongoose');
const findOrCreate = require('mongoose-findorcreate');
const passport = require('passport');
const passportLocalMongoose = require('passport-local-mongoose');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

let port = process.env.PORT;
let dbUrl = 'mongodb+srv://admin-jan:' + process.env.DB_PASSWORD + '@cluster0.js8fa.mongodb.net/secretsDB?retryWrites=true&w=majority';
let callbackURL = "https://secrets-app2.herokuapp.com/auth/google/callback";

const LOCAL = false;
if (LOCAL) {
    port = 5000;
    dbUrl = 'mongodb://localhost:27017/userDB';
    callbackURL = "http://localhost:5000/auth/google/callback";
}



const app = express();
app.set('view engine', 'ejs');


// Global middlewares
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());


mongoose.connect(dbUrl, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
const userSchema = new mongoose.Schema({
    email: String,
    password: String,
    googleId: String,
    secret: String
});
userSchema.plugin(passportLocalMongoose);  // Used to salt and hash our passwords.
userSchema.plugin(findOrCreate);
const User = mongoose.model('User', userSchema);


// In order to support login sessions, Passport will serialize and deserialize
// user instances to and from the session.
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});


// Strategies
passport.use(User.createStrategy());

passport.use(new GoogleStrategy(
    {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: callbackURL
    },
    function(accessToken, refreshToken, profile, cb) {
        User.findOrCreate({ googleId: profile.id }, function (err, user) {
            return cb(err, user);
        });
    })
);


app.get('/', function (req, res) {
    res.render('home');
});


app.get('/auth/google',
    passport.authenticate('google', {scope: ['profile']})
);


app.get('/auth/google/callback',
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect to secrets.
    res.redirect('/secrets');
  });


app.get('/register', function (req, res) {
    res.render('register');
});


app.get('/login', function (req, res) {
    res.render('login');
});


app.get('/secrets', function (req, res) {
    User.find({'secret': {$ne: null}}, function(err, foundUsers) {
        if (err) {
            console.log(err);
        } else {
            if (foundUsers) {
                res.render('secrets', {usersWithSecrets: foundUsers});
            }
        }
    });
});


app.get('/submit', function (req, res) {
    // req.isAuthenticated() will return true if user is logged in.
    if (req.isAuthenticated()) {
        res.render('submit');
    } else {
        res.redirect('/login');
    }
});


app.post('/submit', function (req, res) {
    const submittedSecret = req.body.secret;

    User.findById(req.user.id, function (err, foundUser) {
        if (err) {
            console.log(err);
        } else {
            if (foundUser) {
                foundUser.secret = submittedSecret;
                foundUser.save(function () {
                    res.redirect('/secrets');
                });
            }
        }
    });
});


app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/')
});


app.post('/register', function (req, res) {

    // register() comes from passport-local-mongoose
    User.register({username: req.body.username}, req.body.password, function(err, user) {
        if (err) {
            console.log(err);
            res.redirect('/register');
        } else {
            console.log('user registered');
            // Authenticate user using passport. function() callback is only
            // called if authentication succeeds.
            passport.authenticate('local')(req, res, function() {
                console.log('authentication succeded');
                res.redirect('/secrets');
            });
        }
    });

});


app.post('/login',
    passport.authenticate('local', {failureRedirect: '/login'}),
    function(req, res) {
        res.redirect('/secrets');
    }
);


app.listen(port, function () {
    console.log('Server started on port ' + port);
});
